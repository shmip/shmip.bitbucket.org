$(function() {
    // Get rid of table of contents
    $('#table-of-contents').remove();

    // Hide all the sections
    $('.outline-text-2').hide()

    // Add click listeners to each heading to toggle the visiblity of the associated section
    $('.outline-2 > h2').on('click', function(e) {
        $(e.target).parent().find('.outline-text-2').toggle()
    });

    $('.outline-2 > h2').on('mouseover', function(e) {
        $(e.target).css('border-bottom-color', '#666');
    });

    $('.outline-2 > h2').on('mouseout', function(e) {
        $(e.target).css('border-bottom-color', '#FFF');
    });
})
